# Changelog
All notable changes to this package will be documented in this file.

The format is based on [Keep a Changelog](http://keepachangelog.com/en/1.0.0/)
and this project adheres to [Semantic Versioning](http://semver.org/spec/v2.0.0.html).

## [1.0.1] - 2020-02-19
### Changed
- Correct the way AnimationCurve evaluates the correct time

## [1.0.0] - 2020-02-16
### Added
- Add Light2DIntensityAnimation component
- Add README
- Add initial files
- Initial commit