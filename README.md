# Lights

### What is this package for? ###

* This package contains useful scripts for lights.
* Minimal Unity version: 2018.3

### What can I do with it? ###
* **Light2DIntensityAnimation**: it uses an AnimationCurve to animates a Light2D intensity property.

### How do I get set up? ###
* Using the **Package Registry Server**:
	* Add this line before ```dependencies``` node:
		* ```"scopedRegistries": [ { "name": "Action Code", "url": "http://34.83.179.179:4873/", "scopes": [ "com.actioncode" ] } ],```
	* The package **ActionCode-Lights** will be avaliable for you to intall using the **Package Manager** windows.
* By **Git URL** (you'll need a **Git client** installed on your machine):
	* Add this line inside ```dependencies``` node: 
		* ```"com.actioncode.lights":"https://bitbucket.org/nostgameteam/lights.git"```

### Who do I talk to? ###

* Repo owner and admin: **Hyago Oliveira** (hyagogow@gmail.com)