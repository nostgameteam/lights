﻿using UnityEngine;

namespace ActionCode.Lights
{
    /// <summary>
    /// Abstract component for light curve animation.
    /// </summary>
    [ExecuteAlways]
    [DisallowMultipleComponent]
	public abstract class AbstractCurveAnimation : MonoBehaviour 
	{
        [Tooltip("Should start playing the animation on Awake?")]
        public bool playOnAwake = true;
        [Tooltip("Animates the light using this curve.")]
        public AnimationCurve curve = AnimationCurve.EaseInOut(0f, 0f, 1f, 1f);

        /// <summary>
        /// Current AnimationCurve active time.
        /// </summary>
        public float ActiveTime { get; private set; }

        /// <summary>
        /// Is the animation playing?
        /// </summary>
        public bool IsPlaying { get; private set; }

        protected virtual void Reset()
        {
            curve.postWrapMode = WrapMode.PingPong;
        }

        protected virtual void Awake()
        {
            Stop();
        }

        protected virtual void OnEnable()
        {
            if (playOnAwake) Play();
        }

        private void Update () 
		{
            if (IsPlaying)
            {
                ActiveTime += Time.deltaTime;
                float curveValue = GetCurveValue(ActiveTime);
                UpdateAnimation(curveValue);
            }            
        }

        private void OnDrawGizmos()
        {
            // Ensures animation is played on Edit Mode.
            #if UNITY_EDITOR
            if (!Application.isPlaying)
            {                
                UnityEditor.EditorApplication.QueuePlayerLoopUpdate();
                UnityEditor.SceneView.RepaintAll();
            }
            #endif
        }

        /// <summary>
        /// Plays the animation.
        /// </summary>        
        public void Play()
        {
            ActiveTime = 0f;
            IsPlaying = true;
        }

        /// <summary>
        /// Stops the animation.
        /// </summary>
        public void Stop()
        {
            IsPlaying = false;
            float curveValue = GetCurveValue(0f);
            UpdateAnimation(curveValue);
        }

        /// <summary>
        /// Gets the curve value at the given time.
        /// </summary>
        /// <param name="time">Time to get the curve value.</param>
        /// <returns>The value of the curve, at the point in time specified.</returns>
        public virtual float GetCurveValue(float time)
        {
            return curve.Evaluate(time);
        }

        /// <summary>
        /// Updates the animation using the given curve value.
        /// </summary>
        /// <param name="curveValue">The value of the curve.</param>
        public abstract void UpdateAnimation(float curveValue);

        /// <summary>
        /// Toggles between Play and Stop
        /// </summary>
        [ContextMenu("Toggle Play")]
        public void TogglePlay()
        {
            if (IsPlaying) Stop();
            else Play();
        }
    }
}