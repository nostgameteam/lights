﻿using UnityEngine;
using UnityEngine.Experimental.Rendering.Universal;

namespace ActionCode.Lights
{
    /// <summary>
    /// This component uses an AnimationCurve to animate a Light2D intensity property.
    /// </summary>
    [RequireComponent(typeof(Light2D))]
	public class Light2DIntensityAnimation : AbstractCurveAnimation
    {
        [SerializeField, Tooltip("The light thats will be animated.")]
        private new Light2D light;

        protected override void Reset()
        {
            base.Reset();
            light = GetComponent<Light2D>();
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            EnableLight(true);
        }

        private void OnDisable()
        {
            EnableLight(false);
        }

        public override void UpdateAnimation(float curveValue)
        {
            if (light) light.intensity = curveValue;
        }

        private void EnableLight(bool enable)
        {
            if (light) light.enabled = enable;
        }
    }
}